/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Meluleki Dube
 *
 * Created on 10 May 2018, 12:31
 */
#include "../include/Audio.h"
#include <cstdlib>
#include <map>
#include <iostream>
#include <string>
//Definition for the different operations.
#define add "-add"
#define cut "-cut"
#define radd "-radd"
#define cat "-cat"
#define v "-v"
#define rev "-rev"
#define rms "rms"
#define norm "-norm"
#define outfile_flag "-o"
//End of definitions.

using namespace std;
map<string, int> options; //map to hold the different options and represent them as ints to allow to use switch cases
void process_input(string operation, char ** arguments, int index);
int samplerate;
int bit_count;
int no_channels;
string name = "out";
string audio1, audio2;

/**
 * Method to write the results for the operations to the file
 * @param a resulting autdio
 */
template <typename T>
void write_to_file(const Audio<T>& a) {
    a.saveFile(name);
}

/**
 * method to populate the menu
 */
void populate_options() {
    options.insert(make_pair(add, 0));
    options.insert(make_pair(cut, 1));
    options.insert(make_pair(radd, 2));
    options.insert(make_pair(cat, 3));
    options.insert(make_pair(v, 4));
    options.insert(make_pair(rev, 5));
    options.insert(make_pair(rms, 6));
    options.insert(make_pair(norm, 7));
}

int main(int argc, char** argv) {\
    /*
                                                                                                                                                                                                            samp  -r  sampleRateInHz -b bitCount  -c noChannels [-o outFileName ] [<ops>]  soundFile1 [soundFile2]                                   
     */
    populate_options();
    samplerate = stoi(argv[2]);
    bit_count = stoi(argv[4]);
    no_channels = stoi(argv[6]);

    int index = 6;
    if (string(argv[++index]) == "-o") {
        name = argv[++index];
    }
    string op = argv[index];
    cout << "Sample rate: " << samplerate << endl;
    cout << "bit_count: " << bit_count << endl;
    cout << "number of channels: " << no_channels << endl;
    cout << "outFile: " << name << endl;
    cout << "audio1 is " << audio1 << " Audio2 is " << audio2 << endl;
    process_input(op, argv, index);
    return 0;
}

void process_input(string op, char**arguments, int index) {
    //    --index;
    typedef Audio<int16_t> mono16;
    typedef Audio<int8_t> mono8;
    typedef Audio<std::pair<int16_t, int16_t>> stereo16;
    typedef Audio<std::pair<int8_t, int8_t>> stereo8;
    switch (options[op]) {
        case 0:// add operation
        {
            cout << "operation add selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    mono8 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1 + clip2);
                } else {
                    mono16 clip1(arguments[++index], samplerate, bit_count);
                    mono16 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1 + clip2);
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    stereo8 clip1(arguments[++index], samplerate, bit_count);
                    stereo8 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1 + clip2);
                } else {
                    stereo16 clip1(arguments[++index], samplerate, bit_count);
                    stereo16 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1 + clip2);
                }
            }
            break;
        }
        case 1: //cut operation selected
        {
            cout << "operation cut selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    int val1 = stoi(arguments[++index]), val2 = stoi(arguments[++index]);
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1^(make_pair(val1, val2)));

                } else {
                    int val1 = stoi(arguments[++index]), val2 = stoi(arguments[++index]);
                    cout << arguments[index] << endl;
                    mono16 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1^(make_pair(val1, val2)));
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    int val1 = stoi(arguments[++index]), val2 = stoi(arguments[++index]);
                    stereo8 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1^(make_pair(val1, val2)));
                } else {
                    int val1 = stoi(arguments[++index]), val2 = stoi(arguments[++index]);
                    stereo16 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1^(make_pair(val1, val2)));
                }
            }
            break;
        }
        case 2: // radd operation selected
        {
            cout << "operation radd selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    pair<float, float> r1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    pair<float, float> s1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    mono8 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1.ra(clip2, r1, s1));
                } else {
                    pair<float, float> r1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    pair<float, float> s1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    mono16 clip1(arguments[++index], samplerate, bit_count);
                    mono16 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1.ra(clip2, r1, s1));
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    pair<float, float> r1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    pair<float, float> s1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    stereo8 clip1(arguments[++index], samplerate, bit_count);
                    stereo8 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1.ra(clip2, r1, s1));
                } else {
                    pair<float, float> r1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    pair<float, float> s1(make_pair(stof(arguments[++index]), stof(arguments[++index])));
                    stereo16 clip1(arguments[++index], samplerate, bit_count);
                    stereo16 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1.ra(clip2, r1, s1));
                }
            }
            break;
        }
        case 3: // cat operation selected
        {
            cout << "operation cat selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    mono8 clip2(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1 | clip2);
                } else {
                    mono16 clip1(audio1, samplerate, bit_count);
                    mono16 clip2(audio2, samplerate, bit_count);
                    write_to_file(clip1 | clip2);
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    stereo8 clip1(audio1, samplerate, bit_count);
                    stereo8 clip2(audio2, samplerate, bit_count);
                    write_to_file(clip1 | clip2);
                } else {
                    stereo16 clip1(audio1, samplerate, bit_count);
                    stereo16 clip2(audio2, samplerate, bit_count);
                    write_to_file(clip1 | clip2);
                }
            }
            break;
        }
        case 4: // volume operation
        {
            cout << "operation v selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1 * make_pair(stof(arguments[++index]), stof(arguments[++index])));
                } else {
                    mono16 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1 * make_pair(stof(arguments[++index]), stof(arguments[++index])));
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    stereo8 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1 * make_pair(stof(arguments[++index]), stof(arguments[++index])));
                } else {
                    stereo16 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1 * make_pair(stof(arguments[++index]), stof(arguments[++index])));
                }
            }
            break;
        }
        case 5: //revese operation
        {
            cout << "operation rev selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1.r());
                } else {
                    mono16 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1.r());
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    stereo8 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1.r());
                } else {
                    stereo16 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1.r());
                }
            }
            break;
        }
        case 6: // rms operations
        {
            cout << "operation rms selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    clip1.r();
                } else {
                    mono16 clip1(audio1, samplerate, bit_count);
                    clip1.r();
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    stereo8 clip1(audio1, samplerate, bit_count);
                    clip1.r();
                } else {
                    stereo16 clip1(audio1, samplerate, bit_count);
                    clip1.r();
                }
            }
            break;
        }
        case 7: // normalization operation
        {
            cout << "operation norm selected" << endl;
            if (no_channels == 1) {
                if (bit_count == 8) {
                    mono8 clip1(arguments[++index], samplerate, bit_count);
                    write_to_file(clip1.n(make_pair(stof(arguments[++index]), stof(arguments[++index]))));
                } else {
                    mono16 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1.n(make_pair(stof(arguments[++index]), stof(arguments[++index]))));
                }
            } else if (no_channels == 2) {
                if (bit_count == 8) {
                    stereo8 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1.n(make_pair(stof(arguments[++index]), stof(arguments[++index]))));
                } else {
                    stereo16 clip1(audio1, samplerate, bit_count);
                    write_to_file(clip1.n(make_pair(stof(arguments[++index]), stof(arguments[++index]))));
                }
            }
            break;
        }
    }
}
