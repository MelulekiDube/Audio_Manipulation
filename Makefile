#Make file for Image Processor
#Created on: Thu Feb 19 00:18:08 1970
#Meluleki Dube

#Directories
SRCDIR=src
BUILDDIR=build
TESTDIR= test
BINDIR = bin

CPP =g++
CPPFLAGS =-std=c++11
TARGET=samp
#TEST_TARGET =$(BINDIR)/samp_test
SRC=$(SRCDIR)/main.cpp #$(TESTDIR)/Testingfile.cpp 
OBJECTS=$(BUILDDIR)/main.o 
#TEST_OBJECTS=$(BUILDDIR)/Testingfile.o 
$(TARGET): $(OBJECTS)
	@echo "-------------------------------------------------------------------------------------------";
	@echo "Linking the object files to create imageops.run";
	$(CPP) $(OBJECTS)-o $@ $(CPPFLAGS)  
	@mv $(TARGET) $(BINDIR)
$(OBJECTS): $(SRC)
	@echo "-------------------------------------------------------------------------------------------";
	@echo "Producing Object files";
	$(CPP) -c $(SRC) $(CPPFLAGS)
	@mv *.o $(BUILDDIR)

#test:$(OBJECTS)
#	@echo "-------------------------------------------------------------------------------------------";
#	@echo "Linking the object files to create imageops_test.run";
#	$(CPP) $(TEST_OBJECTS) -o $(TEST_TARGET) $(CPPFLAGS)

run:
	./$(TARGET)

run_test:
	./$(TEST_TARGET)

clean:
	@echo "Cleaning.............";
	rm -f -r $(BUILDDIR)/*.o $(BINDIR)/*.run
# end of Makefile
