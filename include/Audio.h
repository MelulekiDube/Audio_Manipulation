/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Audio.h
 * Author: Meluleki Dube
 *
 * Created on 10 May 2018, 12:32
 */

#ifndef AUDIO_H
#define AUDIO_H

#include <vector>
#include <ostream>
#include <istream>
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <limits>
#include <algorithm>
#include <numeric>
#include <cmath>

template <typename T>
class Audio {
public:
    //        the big six

    Audio() {
    }

    Audio(std::string fn, int sb, int nb) : file_name(fn), sampling_rate(sb), no_bits(nb), no_channels(1), limit(int(std::numeric_limits<T>::max())) {
        /*initilize the rest in here*/
        std::cout << "file is " << file_name << std::endl;
        readFile();
    }

    Audio(const Audio<T> & orig)
    : file_name(orig.file_name), sampling_rate(orig.sampling_rate), no_bits(orig.no_bits), no_channels(orig.no_channels) {
        readFile();
    }

    Audio(Audio<T>&& orig) :
    file_name(std::move(orig.file_name)), sampling_rate(std::move(orig.sampling_rate)), no_bits(std::move(orig.no_bits)),
    no_channels(std::move(orig.no_channels)), data(std::move(orig.data)) {
        file_name = "";
        orig.sampling_rate = orig.no_bits = orig.no_channels = orig.no_samples = 0;
    }

    Audio<T>& operator=(const Audio<T>& orig) {
        file_name = orig.file_name;
        file_name = orig.file_name;
        sampling_rate = orig.sampling_rate;
        no_bits = orig.no_bits;
        no_channels = orig.no_channels;
        readFile();
    }

    Audio<T>& operator=(Audio<T>&& orig) {
        file_name = std::move(orig.file_name);
        sampling_rate = std::move(orig.sampling_rate);
        no_bits = std::move(orig.no_bits);
        no_channels = std::move(orig.no_channels);
        data = std::move(orig.data);
        file_name = "";
        orig.sampling_rate = orig.no_bits = orig.no_channels = orig.no_samples = 0;
    }

    virtual ~Audio() {
    }
    // class methods

    Audio<T> add(const Audio<T>& a) const {
        Audio<T> result(*this);
        for (int i = 0; i < result.no_samples; ++i) {
            int val = result.data[i] + a.data[i];
            result.data[i] = (val > limit ? limit : val);
        }
        return result;
    }

    Audio<T> cut(std::pair<int, int> range) const {
        Audio<T> result;
        int r = range.second - range.first;
        result.no_samples = no_samples - r;
        result.data.resize(result.no_samples);
        int count = 0;
        for (int i = 0; i < no_samples; ++i) {
            if (i < range.first || i > range.second) {
                result.data[count++] = data[i];
            }
        }
        return result;
    }

    Audio<T> ra(const Audio<T>& rhs, std::pair<float, float> range1, std::pair<float, float> range2) const {
        Audio<T> result(*this); // result =*this;
        result.data.resize(range1.second - range1.first);

        //compute ranges from the formula Length of the audio clip in seconds = NumberOfSamples / (float) samplingRate
        std::pair<int, int> r1(int(range1.first * sampling_rate), int(range1.second * sampling_rate));
        std::pair<int, int>r2(int(range2.first * rhs.sampling_rate), int(range2.second * rhs.sampling_rate));

        Audio<T> a1 = *this^r1;
        Audio<T> a2 = rhs^r2;
        return a1 + a2;
    }

    Audio<T> operator^(std::pair<int, int>range) const {
        return cut(range);
    }

    Audio<T> operator+(const Audio<T> rhs) const {
        return add(rhs);
    }

    Audio<T> operator*(std::pair<float, float> F) const {
        Audio<T> result(*this);
        return result.volume(F);
    }

    Audio<T> operator|(const Audio< T>&rhs) const {
        return cat(rhs);
    }

    double rms() const {
        double result = std::accumulate(data.begin(), data.end(),
                0, [](int total, int i) {
                    return total + (i * i); });
        result /= no_samples;
        result = pow(result, 0.5);
        std::cout << " RMS = " << result << std::endl;
        return result;
    }

    template<typename U>
    class normalize {
    private:
        float rms;
        float d;
    public:

        normalize(float r, float _d) : rms(rms), d(_d) {
        }

        double operator()(const U& a) const {
            return a * (d / rms);
        }
    };

    Audio<T> n(std::pair<float, float> des) const {
        Audio<T> result(*this);
        std::transform(result.data.begin(), result.data.end(), result.data.begin(), normalize<T>(rms(), des.first));
    }

    Audio<T> cat(const Audio<T>& rhs)const {
        Audio<T> result(*this);
        result.data.resize(data.size() + rhs.data.size());
        result.no_samples = no_samples + rhs.no_samples;
        for (int i = no_samples; i < result.no_samples; ++i) {
            result.data[i] = rhs.data[i - no_samples];
        }
        return result;
    }

    Audio<T> r() const {
        Audio<T> result(*this);
        std::reverse(result.data.begin(), result.data.end());
        return result;
    }

    Audio<T> volume(std::pair<float, float> F) const {
        Audio result(*this);
        for (int i = 0; i < result.no_samples; ++i) {
            int temp_re = data[i] * F.first;
            result.data[i] = (temp_re > limit) ? limit : temp_re;
        }
        return result;
    }

    /**
     * Reads the file from the given file name to the current audio object
     */
    void readFile() {
        std::ifstream in;
        in.open(file_name, std::ios::binary);
        in.seekg(0, in.end);
        int file_size = in.tellg();
        in.seekg(0, in.beg);
        no_samples = file_size / (sizeof (T) * no_channels);
        data.reserve(no_samples);
        in.read((char*) &data[0], no_samples);
    }

    /**
     * Writing the audio clip data from fector to the file
     * @param file the file name where to save the clip
     */
    void saveFile(std::string file) const {
        std::cout << "writing to file " << file << std::endl;
        std::ofstream out;
        out.open(file + "raw", std::ios::binary | std::ios::out);
        out.write((char*) (&data[0]), no_samples);
        out.close();
    }

private:
    int limit;
    std::vector<T> data;
    std::string file_name;
    int sampling_rate;
    long no_samples;
    int no_bits;
    int no_channels;
};

/*Specialization*/
template <typename T>
class Audio<std::pair<T, T>>
{
    public:
    //        the big six

    Audio() {
    }

    Audio(std::string fn, int sb, int nb) : file_name(fn), sampling_rate(sb), no_bits(nb), no_channels(2) {
        /*initilize the rest in here*/
        std::cout << "file is " << file_name << std::endl;
        readFile();
    }

    Audio(const Audio<std::pair<T, T>> &orig)
            : file_name(orig.file_name), sampling_rate(orig.sampling_rate), no_bits(orig.no_bits), no_channels(orig.no_channels) {
        readFile();
    }

    Audio(Audio<std::pair<T, T>>&& orig) :
            file_name(std::move(orig.file_name)), sampling_rate(std::move(orig.sampling_rate)), no_bits(std::move(orig.no_bits)),
            no_channels(std::move(orig.no_channels)), data(std::move(orig.data)) {
        file_name = "";
        orig.sampling_rate = orig.no_bits = orig.no_channels = orig.no_samples = 0;
    }

    Audio& operator=(const Audio<std::pair<T, T>>&orig) {
        file_name = orig.file_name;
        file_name = orig.file_name;
        sampling_rate = orig.sampling_rate;
        no_bits = orig.no_bits;
        no_channels = orig.no_channels;
        readFile(); // can change to data= rhs.data
    }

    Audio& operator=(Audio<std::pair<T, T>>&& orig) {
        file_name = std::move(orig.file_name);
        sampling_rate = std::move(orig.sampling_rate);
        no_bits = std::move(orig.no_bits);
        no_channels = std::move(orig.no_channels);
        data = std::move(orig.data);
        file_name = "";
        orig.sampling_rate = orig.no_bits = orig.no_channels = orig.no_samples = 0;
    }

    virtual ~Audio() {
    }
    // class methods

    Audio<std::pair < T, T >> add(const Audio<std::pair<T, T>>&a) const {
        int limit = (int(std::numeric_limits<T>::max()));
        Audio<std::pair<T, T >> result(*this);
        for (int i = 0; i < result.no_samples; ++i) {
            int val_left = result.data[i].first + a.data[i].first;
            int val_right = result.data[i].second + a.data[i].second;
            val_left = (val_left > limit ? limit : val_left);
            val_right = (val_right > limit ? limit : val_right);
            result.data[i] = std::make_pair(val_left, val_right);
        }
        return result;
    }

    Audio<std::pair<T, T >> cut(std::pair<int, int> range) const {
        Audio<std::pair<T, T >> result(*this);
        int r = range.second - range.first;
        result.no_samples = no_samples - r;
        result.data.erase(result.data.begin()+ range.first, result.data.begin() + range.second);
        return result;
    }

    Audio<std::pair<T, T >> ra(const Audio& rhs, std::pair<float, float> range1, std::pair<float, float> range2) const {
        Audio<std::pair<T, T >> result(*this); // result =*this;
        result.data.resize(range1.second - range1.first);

        //compute ranges from the formula Length of the audio clip in seconds = NumberOfSamples / (float) samplingRate
        std::pair<int, int> r1(int(range1.first * sampling_rate), int(range1.second * sampling_rate));
        std::pair<int, int>r2(int(range2.first * rhs.sampling_rate), int(range2.second * rhs.sampling_rate));

        Audio<std::pair<T, T>> a1 = *this^r1;
        Audio<std::pair<T, T>> a2 = rhs^r2;
        return a1 + a2;
    }

    Audio<std::pair<T, T>> operator^(std::pair<int, int>range) const {
        return cut(range);
    }

    Audio<std::pair<T, T>> operator+(const Audio & rhs) const {
        return add(rhs);
    }

    Audio<std::pair<T, T>> operator*(std::pair<float, float> F) {
        Audio<std::pair<T, T >> result(*this);
        return result.volume(F);
    }

    Audio<std::pair<T, T>> operator|(const Audio<std::pair<T, T>>&rhs) {
        return cat(rhs);
    }

    std::pair<double, double> rms() {
        std::pair<float, float> res = std::accumulate(data.begin(), data.end(), std::pair<float, float> (0, 0),
                [](std::pair<T, T> left, std::pair<T, T> right) {
                    return std::pair<float, float> (left.first + (right.first * right.first), left.second + (right.second * right.second));
                });
        std::pair<double, double>result = res;
        result.first = pow(result.first, 0.5);
        result.second = pow(result.second, 0.5);
        std::cout << "Left RMS = " << result.first << " Right RMS = " << result.second << std::endl;
        return result;
    }

    template<typename U>
    class normalize {
    private:
        std::pair<float, float> rms;
        std::pair<float, float> d;
    public:

        normalize(std::pair<float, float> r, std::pair<float, float> _d) : rms(rms), d(_d) {
        }

        std::pair<float, float> operator()(const std::pair<U, U>& a) const {
            return std::make_pair(a.first * (d.first / rms.first), a.second * (d.second / rms.first));
        }
    };

    Audio<std::pair<T, T >> n(std::pair<T, T> des) {
        Audio<std::pair<T, T >> result(*this);
        std::transform(result.data.begin(), result.data.end(), result.data.begin(), normalize<T>(rms(), des));
    }

    Audio<std::pair<T, T >> cat(const Audio & rhs)const {
        Audio<std::pair<T, T >> result(*this);
        result.data.resize(data.size() + rhs.data.size());
        result.no_samples = no_samples + rhs.no_samples;
        for (int i = no_samples; i < result.no_samples; ++i) {
            result.data[i].first = rhs.data[i - no_samples].first;
            result.data[i].second = rhs.data[i - no_samples].second;
        }
        return result;
    }

    Audio<std::pair<T, T >> r() {
        Audio<std::pair<T, T >> result(*this);
        std::reverse(result.data.begin(), result.data.end());
        return result;
    }

    Audio<std::pair<T, T >> volume(std::pair<float, float> F) {
        Audio result(*this);
        for (int i = 0; i < result.no_samples; ++i) {
            data[i].first *= F.first;
            data[i].second *= F.second;
        }
        return result;
    }

    /**
     * Reads the file from the given file name to the current audio object
     */
    void readFile() {
        std::ifstream in;
        in.open(file_name, std::ios::binary);
        if (!in)
            throw std::invalid_argument("File has an error!!!!!");
        in.seekg(0, in.end);
        int file_size = in.tellg();
        in.seekg(0, in.beg);
        no_samples = file_size / (no_bits * no_channels);
        data.reserve(no_samples);
        T temp_left, temp_right;
        for (int i = 0; i < no_samples; i++) {
            in.read((char*) &temp_left, sizeof (T));
            in.read((char*) &temp_right, sizeof (T));
            data.push_back(std::make_pair(temp_left, temp_right));
        }
    }

    /**
     * Writing the audio to a file
     * @param name the name of the file to save the audio to
     */
    void saveFile(std::string name) const {
        std::ofstream out;
        std::cout << "writing to file " << name << std::endl;
        out.open(name + ".raw", std::ios::binary | std::ios::out);
        for (int i = 0; i < no_samples; ++i) {
            out.write((char*) (&data[i].first), sizeof (T));
            out.write((char*) (&data[i].second), sizeof (T));
        }
        out.close();
    }

    private:
    std::vector<std::pair<T, T>> data;
    std::string file_name;
    int sampling_rate;
    long no_samples;
    int no_bits;
    int no_channels = 2;
};

#endif /* AUDIO_H */

